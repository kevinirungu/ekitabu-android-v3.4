const Promise = require('bluebird'),
      adb = require('adbkit'),
      ApkReader = require('adbkit-apkreader'),
      fs = require('fs-extra'),
      //csvparse = require('csv-parse/lib/sync'),
      csvstringify = require('csv-stringify/lib/sync')
      he = require('he'),
      readdir = require('fs-readdir-recursive'),
      path = require('path'),
      dateFormat = require('dateformat'),
      mkdirp = require('mkdirp'),
      compareVersions = require('compare-versions'),
      ini = require('ini'),
      crypto = require('crypto'),
      BSON = require('bson'),
      JSZip = require('jszip'),
      ArgumentParser = require('argparse').ArgumentParser,
      ChildProcess = require('child_process');

var client = adb.createClient();

var argParser = new ArgumentParser({
    addHelp: true,
    description: `Provisions devices with encrypted epubs`
});

subparsers = argParser.addSubparsers({
    title: 'Subcommands (show additional help with <subcommand> -h)',
    dest: 'command'
}),
encryptSubparser = subparsers.addParser('encrypt', {
    addHelp: true,
    description: 'Encrypts input epubs content and generates content keys, written in the epub'
}),
provisionSubparser = subparsers.addParser('provision', {
    addHelp: true,
    description: 'Encrypts content keys per epub per device and provisions the connected device'
});
installParser = subparsers.addParser('reinstall',{
    addHelp: true,
    description: 'Reinstalls the eKitabu app on the attached device'
});

encryptSubparser.addArgument('--inputDir', {
    help: `Directory containing epubs for encryption. Required.`,
    required: true
});

encryptSubparser.addArgument('--outputDir', {
    help: `Path to directory where encrypted epubs will be stored. Required.`,
    required: true
});

encryptSubparser.addArgument('--deviceIds',{
    help: `Path to device ids list for pre-encryption. Format must be a list of ids on new lines. Optional.`
});

provisionSubparser.addArgument('--inputDir', {
    help: `Directory containing epubs for provisioning. Required.`,
    required: true
});

// provisionSubparser.addArgument('--syncData', {
//     help: `Sync data file. Optional. Default is syncdata.csv`,
//     defaultValue: 'syncdata.csv'
// });

provisionSubparser.addArgument('--indexFile', {
    help: `Category tagging information. Required.`,
    required: true
});

provisionSubparser.addArgument('--testSync', {
  nargs: 0,
  help: `Syncing will be to test account test_b2db and no account will be generated. Optional.`
});

provisionSubparser.addArgument('--overrideSync', {
  nargs: 0,
  help: `Generate new sync account even if present on device. Optional.`
});

provisionSubparser.addArgument('--useKeystore', {
    nargs: 0,
    help: `Use keystore for pre-encrypted keys and skip key encryption.`
});

// provisionSubparser.addArgument('--log', {
//     help: `Path to log file. Optional.`,
//     defaultValue: 'logfile.csv'
// });

installParser.addArgument('--apk',{
    help: 'Path to APK. Default is ekitabu-android-armv7-release.apk.',
    defaultValue: 'ekitabu-android-armv7-release.apk'
});

var args = argParser.parseArgs();

var inputDir = args.inputDir;
var outputDir = args.outputDir;
var apk = args.apk || 'ekitabu-android-armv7-release.apk';
var log = 'books-log.csv'; //args.log;
var deviceLog = 'devices-log.csv';
//var syncData = args.syncData || 'syncdata.csv';

var config = ini.parse(fs.readFileSync('./config.ini', 'utf-8'));

process.on('unhandledRejection', (reason, p) => {
  console.log('An error occured, reason: ', reason);
  // application specific logging, throwing an error, or other logic here
});

if (args.command == "encrypt")
  encryptBooks();
else if (args.command == "provision")
  provisionDevices();
else if (args.command == "reinstall")
  reinstallApp();

function getImei(deviceId) {
  return client.shell(deviceId, 'service call iphonesubinfo 1')
    .then(adb.util.readAll)
    .then(function(output) {
      let imei = output.toString().match(/'[.\d\s]+'/g).join('').replace(/['. ]/g,'');
      return imei;
    });
}

function reinstallApp() {
  client.listDevices()
  .then(function(devices) {
    console.log(`${devices.length} devices connected.`);
    if (!devices.length) return Promise.resolve();
    let device = devices[0]; //assume one connected device
    console.log(`Installing ${apk} on device ${device.id} ...`);
    client.install(device.id, apk, (e) => {
      if (e) console.log(`Error installing APK: ${e}`);
      else console.log(`App reinstalled on device`);
    });
  });
}

function encryptBooks() {
  let epubs = readdir(inputDir);
  epubs = epubs.filter(file => file.endsWith('.epub'));
  console.log(`${epubs.length} EPUB books found in input directory. Processing...`);

  return Promise.map(epubs, epub => {

    console.log(`Processing file: ${epub}`);

    let dirname = path.dirname(`${outputDir}/${path.basename(epub)}`);
    mkdirp.sync(dirname);

    return new Promise((resolve,reject)=>{
      let args = [
            '-input',`${inputDir}/${epub}`,
            '-output',`${outputDir}/${path.basename(epub)}`
      ];

      let proc = ChildProcess.spawn('./go/bin/lcpencrypt', args);
      let result = '';
      proc.stdout.on('data', (log) => {
        result = result + log;
      });
      proc.stderr.on('data', (log) => {
        console.log(`Error output during encryption [${epub}]: ${log.toString()}`);
      });
      proc.on('error',(err)=>{
        console.log(`Error during encryption [${epub}]: ${err}`);
        reject(err);
      });
      proc.on('close', (code, signal) => {
          if (code || signal) {
              if (!result) {
                console.log(`Encryption failed for epub: ${epub}`);
                resolve(null);
                return;
              }
              try {
                //console.log("JSON: ",json);
                let resultJSON = JSON.parse(result);
                console.log(`Error processing ${resultJSON['protected-content-disposition']}: ${resultJSON.error}`);
                resolve(null);
              } catch(e) {
                console.log(e);
                resolve(null);
              }
          } else {
              resolve(result);
          }
      });
    }).then((result)=>{

      if (!result) return Promise.resolve({});

      let resultJSON = JSON.parse(result);
      let contentKey = resultJSON['content-encryption-key'];
      let encryptedEpub = resultJSON['protected-content-location'];

      return new Promise((resolve,reject)=>{
        let data = fs.readFileSync(encryptedEpub);
        JSZip.loadAsync(data).then((zip)=>{
          zip.file('.key',contentKey);
          zip.generateNodeStream({type:'nodebuffer',streamFiles:true})
            .pipe(fs.createWriteStream(encryptedEpub))
            .on('finish', function () {
              console.log("Finished encrypting file: ", epub);
              resolve({title:path.basename(encryptedEpub),contentKey:contentKey});
            });
        });
      }).catch((err) => {
          console.log("Error processing archive: ", epub);
          console.log(err);
          resolve({}); //dont break on a faulty epub
      });
    });
  },{concurrency:20})
  .then(function(contentKeys) {
    console.log(`Done encrypting. Files are in: ./${outputDir}`);

    if (args.deviceIds) {
      let deviceIds = fs.readFileSync(args.deviceIds,'utf-8');//openfile
      let outputKeyFile = `${outputDir}/.service`;
      deviceIdsArr = deviceIds.split(/\s+/);
      let keyStore = {};
      for (let d=0;d<deviceIdsArr.length;d++) {
          let deviceId = deviceIdsArr[d];
          keyStore[deviceId] = {};
          for (let k=0;k<contentKeys.length;k++) {
            let key = contentKeys[k];
            if (!key.title) continue; //guard against empty epubs
            keyStore[deviceId][key.title] = encrypt(deviceId,key.contentKey);
          }
      }

      let bson = new BSON();

      fs.writeFileSync(outputKeyFile,bson.serialize(keyStore));

      console.log(`Writing pre-encrypted keys to keystore file`);
    }

  })
  .catch(function(err) {
    console.error('Something went wrong: ', err);
  });
}

function provisionDevices() {

  let logs = [];
  let epubs = [];
  let device = null;
  let imei = null;
  let version = null;

  return client.listDevices()
  .then(function(devices) {

    console.log(`${devices.length} devices connected.`);
    if (!devices.length) return Promise.resolve();
    device = devices[0]; //assume one connected device
    console.log(`Processing device with ID: ${device.id}`);

    epubs = readdir(inputDir);
    epubs = epubs.filter(file => file.endsWith('.epub'));
    console.log(`${epubs.length} EPUB books found in input directory.`);

    if (!epubs.length) return Promise.resolve();

    return client.isInstalled(device.id,'com.ekitabu.mobile');
  })
  .then((installed)=>{
    return ApkReader.open(apk)
      .then(reader => reader.readManifest())
      .then(manifest => {
        version = manifest.versionName;
        if (installed) {
          return client.shell(device.id, 'dumpsys package com.ekitabu.mobile')
          .then(adb.util.readAll)
          .then(function(output) {
            let versionOnDevice = output.toString().match(/versionName=(\d+\.\d+\.\d+)/)[1];
            if (compareVersions(versionOnDevice,version) < 0) {
              console.log(`Updating eKitabu mobile on device to version ${version}...`);
              return client.install(device.id, apk, (e) => { if (e) console.log(e)});
            }
            else {
              version = versionOnDevice;
              console.log("eKitabu mobile already installed on device");
              return Promise.resolve();
            }
          });
        }
        else {
          console.log(`Installing eKitabu mobile to device...`);
          return client.install(device.id, apk, (e) => { if (e) console.log(e)});
        }
      });
  }).then(()=>{
    return getImei(device.id);
  }).then((deviceImei)=>{
    console.log(`Device IMEI: ${deviceImei}`);
    imei = deviceImei;

    let keys = null;

    if (args.useKeystore) {

      if (!fs.exists(`${inputDir}/.service`)) {
        //console.log("Keystore file not found in input directory.");
        return Promise.reject("Keystore file not found in input directory.");
      } else {
        //todo check for file exist
        let bson = new BSON();
        let keyStore = bson.deserialize(fs.readFileSync(`${inputDir}/.service`));
        keys = keyStore[device.id];
        if (keys)
          console.log("Using pre-encrypted keys");
        else {
          //console.log("Keys for this device were not found in the keystore.");
          return Promise.reject("Keys for this device were not found in the keystore.");
        }
      }
    }

    return Promise.map(epubs, epub => {
      console.log(`Provisioning file: ${epub}`);
      let dirname = path.dirname(epub);

      let metadata = {};

      return new Promise((resolve,reject)=>{
        let data = fs.readFileSync(`${inputDir}/${epub}`);
        JSZip.loadAsync(data,{base64:true})
        .then((zip)=>{
          let contentFile = zip.file(/[^.+]\.opf/)[0];
          if (!contentFile) {
            metadata.title = metadata.publisher = metadata.identifier = "[MISSING OPF FILE]";
            return Promise.resolve();
          }
          return contentFile.async('text').
          then ((opf) => {
            let titleMatch = opf.match(/<dc:title[^>]*>([^<]*)<\/dc:title>/);
            metadata.title = titleMatch && titleMatch.length ? he.decode(titleMatch[1]) : "[MISSING]";
            let publisherMatch = opf.match(/<dc:publisher[^>]*>([^<]*)<\/dc:publisher>/);
            metadata.publisher = publisherMatch && publisherMatch.length ? he.decode(publisherMatch[1]) : "[MISSING]";
            let idMatch = opf.match(/<dc:identifier[^>]*>([^<]*)<\/dc:identifier>/);
            metadata.identifier = idMatch && idMatch.length ? he.decode(idMatch[1]) : "[MISSING]";
          }).
          then(()=> zip.file('.key').async('text')).
          then((contentKey)=> {
            let encryptedKey = null;
            if (keys && keys[path.basename(epub)]) {
              console.log("Using pre-encrypted key for epub: ",path.basename(epub));
              encryptedKey = keys[path.basename(epub)];
            } else {
              encryptedKey = encrypt(device.id, contentKey);
            }
            zip.file('.key',encryptedKey);
            mkdirp.sync(`tmp/${device.id}/${dirname}`);
            zip.generateNodeStream({type:'nodebuffer',streamFiles:true})
              .pipe(fs.createWriteStream(`tmp/${device.id}/${epub}`))
              .on('error', err => {
                console.log("Error writing zipfile: ",err);
                reject(err);
              })
              .on('finish', () => {
                 console.log(`Finished writing encrypted .key file to epub ${epub}`);
                  resolve();
              });
          })
        }).catch((err)=>{
          console.log("Error handling ZIP: ", err);
          reject(err);
        });
      }).then(()=>{
        //STEP 5 - push the new EPUB to the device
        console.log(`Pushing provisioned file: ${epub}...`);
        return client.push(device.id, `./tmp/${device.id}/${epub}`, `/sdcard/eKitabu/${epub}`);
      }).then(transfer=>{
        return new Promise(function(resolve, reject) {
          transfer.on('progress', function(stats) {
            // console.log('[%s] Pushed %d bytes so far',
            //   device.id,
            //   stats.bytesTransferred)
          });
          transfer.on('end', function() {
            console.log(`[%s] Push complete for ${epub}`, device.id);
            logs.push([
              config.info.projectID,config.info.package,dateFormat(Date.now(), "dd-mm-yyyy HH:MM"),
              /*dateFormat(Date.now(), "dd-mm-yyyy HH:MM"),*/device.id,path.basename(epub),epub,
              metadata.title,metadata.publisher,metadata.identifier]);
            resolve();
          });
          transfer.on('error', reject)
        });
      }).catch((err) => {
        console.error('Error working with device for epub: ',epub,'; error: ',err);
      });

    },{concurrency:50});
  }).then(() => {
    console.log('Pushing index file to device');
    let tstamp = Date.now().toString();
    return client.push(device.id, args.indexFile, `/sdcard/eKitabu/${tstamp}-index.csv`);
  }).then(() => {
    return client.readdir(device.id,'/sdcard/eKitabu').
      then((fileStats)=>{
        let files = fileStats.map((f)=>f.name);
        if (args.overrideSync || !files.includes('.sync')) {
          let syncAcc = {
            user: args.testSync ? 'test_b2db' : `e${device.id}`,
            pass: args.testSync ? '41bd3d' : crypto.randomBytes(12).toString('base64')
          };

          fs.writeFileSync(`.sync`,JSON.stringify(syncAcc));
          console.log('Pushing sync data to device');
          return client.push(device.id, `.sync`, `/sdcard/eKitabu/.sync`).
          then(()=>{
            fs.removeSync('.sync');
            return Promise.resolve(syncAcc);
          });
        } else {
          console.log('Sync data already on device');
          return Promise.resolve(null);
        }
      });
  })
  .then((newsync) => {
    return new Promise((resolve,reject)=>{
      console.log("Writing to book log...");
      fs.appendFileSync(log,`\nStarting log at ${dateFormat(Date.now(),'dd-mm-yyyy HH:MM')}\n\n`);
      fs.appendFileSync(log,csvstringify(logs, {delimiter:','}));
      if (newsync) {
          console.log("Writing to device log...");

          fs.appendFileSync(deviceLog,csvstringify([{
            date: dateFormat(Date.now(),'dd-mm-yyyy HH:MM'),
            serialNo: device.id,
            imei: imei,
            version: version,
            user: newsync.user,
            pass: newsync.pass
          }], {delimiter:','}));
      }
      resolve();
    });
  })
  .then(function() {
    fs.removeSync('./tmp');
    console.log('Done pushing & installing to all connected devices');
  })
  .catch(function(err) {
    console.error('Something went wrong:', err)
  });
}

/**
 * Encrypt plaintext with sha256(key) using AES-256-CBC.
 * @param {string} key The key (passphrase) to use. May be of arbitrary length.
 * @param {string} plaintext The message to encrypt.
 * @returns {string} A Base64-encoded ciphertext.
 */
function encrypt(key, plaintext) {
  const algorithm = 'aes-256-cbc';

  const sha256 = crypto.createHash('sha256');
  sha256.update(key);

  const iv = crypto.randomBytes(16);
  const plaintextBuffer = new Buffer(plaintext);
  const cipher = crypto.createCipheriv(algorithm, sha256.digest(), iv);

  let ciphertext = cipher.update(plaintextBuffer);
  ciphertext = Buffer.concat([iv, ciphertext, cipher.final()]);

  return ciphertext.toString('base64');
}
