# eKitabu installation & usage instructions

## Installation

Extract the distribution archive in a folder anywhere on your hard-disk space.

#### Ubuntu

* Run the **install.sh** script

#### Windows

* Right-click the **install.ps1** script provided.
* If present, choose "Run with PowerShell".
* If not present, choose "Open with..", select 'Choose Default Program', then 'Look for another app..', the navigate to 'C:\Windows\System32\Windows PowerShell\powershell.exe' and select it. Then double-click **install.ps1**
* If prompted to allow changing security policy, type 'Y' and hit Enter.
* When prompted to run with administrative priviliges, choose 'Yes'
* If you have Node.js installed, you can use the existing installation, provided it is a higher version than Node 7.0.0
* When prompted to delete the installation files, choose 'Y'
* Make sure to restart your command prompt after installation

**NOTE**: In case the install script is run in a virtual machine or an otherwise rights restrictive environment, try running `Set-ExecutionPolicy -Scope Process -ExecutionPolicy Bypass` in Powershell prompt before running the install script from the prompt.

## Encrypting the e-books

**NOTE**: make sure to edit the `config.ini` file to fit the current project and package name.

* In the folder where the files have been extracted, create a folder with non-encrypted epubs, e.g. `epubs`, and copy all necessary epubs there
* Open a command prompt or PowerShell prompt and navigate to the foler where the files have been extracted.
* Run `npm start -- encrypt --inputDir epubs --outputDir encrypted` to encrypt the content of all epubs. This will write the encrypted files in a folder called `encrypted` without preserving the directory structure of the input directory.

**NOTE:** you only need to run this step once per a set of ebooks.

* Add the `--deviceIds` argument to the script in order to point to a list of device ids for pre-encryption. Device ids are expected as Android serial numbers. A device ID can be found out by running `adb devices` with the device connected. The device ids list should be textfile with each new device id on a new line.

## Setting up your device

* From the main screen of your Android device, open the Settings app and choose 'About Tablet' from the menu.
* Go to 'Software Information' and tap **7** times on the 'Build number' option. A new 'Developer Options' menu item should appear on the right.
* Open the 'Developer Options' item and scroll down to the 'Debugging' sections. Turn **ON** the USB debugging switch.
* Connect your device via USB.
* From the Terminal/Console shell, type `adb shell`
* Allow the USB connection on the device (a popup should appear) and check "Always allow from this computer"

## Provisioning books on the device

* Open a command prompt or PowerShell prompt and navigate to the folder where the files have been extracted.
* Run `npm start -- provision --inputDir encrypted --indexFile index.csv` to provision all books from the `encrypted` folder to the device.
* The `--indexFile` arguments points to a CSV file containing the bookshelf information for the encrypted books. Note that any book not in the index file will not appear in the mobile app. This argument is **required**.

**NOTE:** the **inputDir** parameter should point to the same folder where you have stored the encrypted books in the previous step, in this case `encrypted`

* In case you want to use the pre-encrypted keys, add the `--useKeystore` parameter. This is possible only if you have supplied the `--deviceIds` argument during encryption.
* Logging of provisioned books is in `books-log.csv`.
* For each new device that is being provisioned, a sync account will be generated and stored on the device and in the `devices-log.csv`. In case the device has already been provisioned at least once, no new account will be generated and no new record will be added to the device log file.
* The above behavior can be overriden with the option `--overrideSync`. This will force creating a new account and overriding it on the device, and it will add a new record to the device log file.

**NOTE:** the provisioning script will automatically replace the mobile app on the device in case it is older than the version in the current package.

## Reinstalling the eKitabu app only

* Open a command prompt or PowerShell prompt and navigate to the folder where the files have been extracted.
* Connect the Android device.
* Run `npm start -- reinstall` to reinstall the eKitabu app on the connected device. You can additionally use the `--apk` parameter to point to the APK with the eKitabu installation. By default, this is the supplied APK, ekitabu-android-armv7-release.apk
